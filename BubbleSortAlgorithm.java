package PracticeSort;

public class BubbleSortAlgorithm {

    public static void main(String[] args) {
        int a[] = {19, 26, 11, 5, 3, 8, 0, 12};
        int t ;

        System.out.println("Before Sorting : ");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i] + " ");

        }
        for (int i = 0; i < a.length; i++) {
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < a[i]) {
                    t = a[i];
                    a[i] = a[j];
                    a[j] = t;
                }
            }
            
        }

        System.out.println("After Sorting : ");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i] + " ");
        }
    }

}
