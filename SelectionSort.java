package PracticeSort;

public class SelectionSort {

    public static void main(String[] args) {
        int a[] = {9, 14, 3, 1, 11};

        System.out.println("Before :");
        printArray(a);
        System.out.println(" ");

        for (int i = 0; i < a.length - 1; i++) {
            int lowIndex = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[lowIndex] > a[j]) {
                    lowIndex = j;
                }
            }
            int temp = a[i];
                a[i] = a[lowIndex];
                a[lowIndex] = temp;
        }

        System.out.println("After :");
        printArray(a);
        System.out.println(" ");
    }

    public static void printArray(int[] input) {
        for (int t : input) {
            System.out.println(t + " ");
        }
    }
}
