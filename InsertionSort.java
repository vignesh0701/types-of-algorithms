package PracticeSort;

public class InsertionSort {
    //the array is virtually split into a sorted and unsorted part,
    //then values from the unsorted part are picked and placed at the correct 
    // position.
    
    public static void main(String[] args) {

        int a[] = {3, 2, 5, 1, 8, 6};

        System.out.println("Before Sorting :");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
        // here loops starts at aray 1 to array n 
        
        for (int i = 1; i < a.length; i++) {
            int key = a[i];
            int j = i - 1;
            while (j >= 0 && a[j] > key) {
                a[j + 1] = a[j];
                j--;
            }
            a[j + 1] = key;
        }
        System.out.println("After Sorting :");
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }

}
